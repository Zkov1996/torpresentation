﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ColoredGroup : MonoBehaviour
{
    [SerializeField] private List<Graphic> _graphics = new List<Graphic>();
    
    public Color color;

    private void Update()
    {
        foreach (var graphic in _graphics)
        {
            graphic.color = color;
        }
    }
}
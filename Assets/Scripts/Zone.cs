﻿using System;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class Zone : MonoBehaviour
{
    [SerializeField] private RectTransform _selfTransform;
    [SerializeField] private RectTransform _colorTranmsform;
    [SerializeField] private RectTransform _maskTranmsform;
    [SerializeField] private Image _colorImage;
    [SerializeField] private Image _maskImage;

    public ZoneData ZoneParameters;
    // public Vector2 Center => _selfTransform.rect.center;
    public RectTransform RectTransform => _selfTransform;

    private void Update()
    {
        _selfTransform.sizeDelta = _colorTranmsform.sizeDelta = new Vector2(1, 1) * ZoneParameters.Radius * 2;
        _maskTranmsform.sizeDelta = new Vector2(1, 1) * (ZoneParameters.Radius - ZoneParameters.Width) * 2;
        _colorImage.color = ZoneParameters.Color;
        _maskImage.color = ZoneParameters.MaskColor;
    }

    [Serializable]
    public struct ZoneData
    {
        [Range(1, 512)] public int Radius;
        [Range(1, 512)] public int Width;
        public Color Color;
        public Color MaskColor;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using CubeTargetingSystem;
using CubeTargetingSystem.Targets.Realization.ZoneTarget;
using UI;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ConnectionsController2 : MonoBehaviour
{
    [SerializeField] private RectTransform _lastConnectionContainer;
    [SerializeField] private RectTransform _lastConnection;
    [SerializeField] private Image _lastConnectionImage;
    [SerializeField] private bool _useLast;

    [SerializeField] private List<ConnectionAndTargets> _links;


    [Range(0, 1)] public float Progress;

    public Color Color;

    private void Awake()
    {
        if (TargetingSystem.Instance != null)
        {
            foreach (var connectionAndTargets in _links)
            {
                connectionAndTargets.Connection.Target1 =
                    connectionAndTargets.From != null
                        ? connectionAndTargets.From
                        : TargetingSystem.Instance?.GetTarget<ZoneMonoTarget>(connectionAndTargets.TargetIdFrom)
                            ?.TrigPos;
                connectionAndTargets.Connection.Target2 =
                    connectionAndTargets.To != null
                        ? connectionAndTargets.To
                        : TargetingSystem.Instance?.GetTarget<ZoneMonoTarget>(connectionAndTargets.TargetIdTo)?.TrigPos;

                connectionAndTargets.Connection.Progress = connectionAndTargets.ProgressComplete ? 1 : 0;
            }
        }

        _lastConnection.localScale = new Vector3(0, 1, 1);
    }

    private void Update()
    {
        var count = _links.Count(targets => !targets.ProgressComplete) + (_useLast ? 1 : 0);
        foreach (var connectionAndTargetsCount in _links.Where(targets => !targets.ProgressComplete)
            .Select((targets, i) => (targets, i)))
        {
            var connectionAndTargets = connectionAndTargetsCount.Item1;
            var index = connectionAndTargetsCount.Item2;
            connectionAndTargets.Connection.Progress = Mathf.Clamp01(Progress * count - index);
        }

        if (TargetingSystem.Instance != null && _useLast)
        {
            var connectionAndTargets = _links.Last();
            var rectTransform =
                connectionAndTargets.To != null
                    ? connectionAndTargets.To
                    : TargetingSystem.Instance?.GetTarget<ZoneMonoTarget>(connectionAndTargets.TargetIdTo)?.TrigPos;

            Vector2 t1 =
                ((Vector2) _lastConnectionContainer.InverseTransformPoint(
                    rectTransform.TransformPoint(rectTransform.rect.center)))
                .normalized;
            float angle1 = Mathf.Acos(Vector2.Dot(Vector2.right, t1)) * Mathf.Sign(t1.y) * 0.5f / Mathf.PI * 360;
            _lastConnectionContainer.Rotate(0, 0, angle1);
            _lastConnection.localScale = new Vector3(Mathf.Clamp01(Progress * count - 1), 1, 1);
            
            _lastConnection.sizeDelta = new Vector2(
                ((Vector2) _lastConnectionContainer.InverseTransformPoint(rectTransform.TransformPoint(rectTransform.rect.center))).magnitude,
                _lastConnection.sizeDelta.y
            );
        }

        _lastConnectionImage.color = Color;
        foreach (var connectionAndTargets in _links)
        {
            connectionAndTargets.Connection.Color = Color;
        }
    }

    [Serializable]
    public class ConnectionAndTargets
    {
        public ConnectionLine Connection;
        public string TargetIdFrom;
        public RectTransform From;
        public string TargetIdTo;
        public RectTransform To;
        public bool ProgressComplete;
    }
}
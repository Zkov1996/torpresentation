﻿using System;
using Screens;
using UnityEngine;
using UnityEngine.UI;

public class ResetButton:MonoBehaviour
{
    [SerializeField] private Button _button;

    private void Awake()
    {
        _button.onClick.AddListener(Call);
    }

    private void Call()
    {
        SlideManager.Instance.Reset();
    }
}
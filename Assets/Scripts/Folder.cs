﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class Folder : MonoBehaviour
{
    [SerializeField] private RectTransform _selfTransform;
    [SerializeField] private Image _bgImage;
    [SerializeField] private Image _contourImage;

    [Range(1, 512)] public int size = 512;
    
    public Color color = Color.white;
    public Color contourColor = Color.black;

    private void Update()
    {
        _selfTransform.sizeDelta = new Vector2(1, 1) * size;
        _bgImage.color = color;
        _contourImage.color = contourColor;
    }
}
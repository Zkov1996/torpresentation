﻿using Translations;
using UnityEngine;
using UnityEngine.UI;

public class LangButton : MonoBehaviour
{
    [SerializeField] private Button _button;

    private void Awake()
    {
        // _button.onClick.AddListener(OnButtonClicked);
    }

    private void OnButtonClicked()
    {
        Localizer.SetLanguage(
            Localizer.Languages[
                (
                    Localizer.Languages
                        .FindIndex(info => info.LanguageCode == Localizer.CurrentLanguage)
                    + 1
                ) % Localizer.Languages.Count
            ].LanguageCode
        );
    }
}
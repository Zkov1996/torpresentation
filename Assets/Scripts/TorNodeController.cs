﻿using System.Collections.Generic;
using CubeTargetingSystem;
using CubeTargetingSystem.Targets.Realization.ZoneTarget;
using UnityEngine;

public class TorNodeController : MonoBehaviour
{
    [SerializeField] private RectTransform _container;
    [SerializeField] private RectTransform _pointerTemplate;
    [SerializeField] private List<string> TargetIds;
    
    public Color Color = Color.white;

    private List<(RectTransform, Vector3, TorNode)> _repositioning = new List<(RectTransform, Vector3, TorNode)>();
    private void Awake()
    {
        if (TargetingSystem.Instance != null)
        {
            foreach (var targetId in TargetIds)
            {
                var target = TargetingSystem.Instance?.GetTarget<ZoneMonoTarget>(targetId);
                var rectTransform = Instantiate(_pointerTemplate, _container, true);
                rectTransform.gameObject.SetActive(false);
                _repositioning.Add((rectTransform, target.TrigPos.position, rectTransform.GetComponent<TorNode>()));
                rectTransform.position = target.TrigPos.position;
                Debug.Log(rectTransform.position);
            }
        }
    }

    private void Update()
    {
        foreach (var valueTuple in _repositioning)
        {
            valueTuple.Item1.gameObject.SetActive(true);
            valueTuple.Item1.position = valueTuple.Item2;
            valueTuple.Item3.Color = Color;
        }
    }
}
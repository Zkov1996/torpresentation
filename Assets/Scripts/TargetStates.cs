﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class TargetStates : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private List<StateToAnimation> _states;


    private static Dictionary<TargetState, int?> _statesDict;

    private TargetState _oldState;
    public TargetState state;

    private void Awake()
    {
        _statesDict ??= _states.ToDictionary(toAnimation => toAnimation.State, toAnimation =>
        {
            try
            {
                return Animator.StringToHash(toAnimation.AnimationName);
            }
            catch
            {
                return (int?) null;
            }
        });
    }

    private void Update()
    {
        if (_oldState != state)
        {
            _oldState = state;
            SetState(state);
        }
    }

    public void SetState(TargetState state)
    {
        if (_statesDict[state].HasValue)
        {
            _animator.Play(_statesDict[state].Value);
        }
    }

    public enum TargetState
    {
        None = 0,
        New = 1,
        Selected = 2,
        Lost = 3,
    }

    [Serializable]
    public class StateToAnimation
    {
        public TargetState State;
        public string AnimationName;
    }
}
﻿using System;
using CubeTargetingSystem.Targets;

namespace CubeTargetingSystem.Infos
{
    [Serializable]
    public struct AddTargetInfo
    {
        public string Id;
        public TargetType Type;
        public BaseTargetCreateParameters Parameters;
    }
}
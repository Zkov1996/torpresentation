﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CoroutineUtils;
using CubeTargetingSystem.Event;
using CubeTargetingSystem.Infos;
using CubeTargetingSystem.Pointers;
using CubeTargetingSystem.Pointers.Interfaces;
using CubeTargetingSystem.Targets;
using CubeTargetingSystem.Targets.Interfaces;
using UnityEngine;
using PointerType = CubeTargetingSystem.Pointers.PointerType;

namespace CubeTargetingSystem
{
    public class TargetingSystem : MonoBehaviour
    {
        [SerializeField] private RectTransform _warmupContainer;
        [SerializeField] private RectTransform _container;

        // public RectTransform Container => _container;
        public static TargetingSystem Instance { get; private set; }

        private TargetsDB _targetsDB;
        private TargetsDB TargetsDB => _targetsDB ??= TargetsDB.Load();

        private Dictionary<string, ITarget> Targets = new Dictionary<string, ITarget>();

        private Dictionary<string, Dictionary<TargetEventType, Action<string>>> _events =
            new Dictionary<string, Dictionary<TargetEventType, Action<string>>>();

        private List<IPointer> _pointers =
            Enumerable.Range(0, 9)
                .Select(i => new CubePointer(i))
                .Append((IPointer) new MousePointer(0)).ToList();

        private Dictionary<PointerType, Dictionary<int, IPointer>> _pointersDict;

        private Dictionary<PointerType, Dictionary<int, IPointer>> pointersDict =>
            _pointersDict ??= _pointers
                .GroupBy(pointer => pointer.Type)
                .ToDictionary(
                    pointers => pointers.Key,
                    pointers => pointers.ToDictionary(pointer => pointer.Index)
                );

        private void Awake()
        {
            Instance = this;
        }

        public void AddEvent(TargetEventInfo info)
        {
            AddEvent(info.Id, info.Type, info.Event.Invoke);
        }

        public void AddEvent(string id, TargetEventType type, Action<string> callback)
        {
            if (!_events.ContainsKey(id))
            {
                _events[id] = new Dictionary<TargetEventType, Action<string>>();
            }

            if (!_events[id].ContainsKey(type))
            {
                _events[id][type] = null;
            }

            _events[id][type] += callback;
        }

        public void RemoveEvent(TargetEventInfo info)
        {
            RemoveEvent(info.Id, info.Type, info.Event.Invoke);
        }

        public void RemoveEvent(string id, TargetEventType type, Action<string> callback)
        {
            if (_events.ContainsKey(id) && _events[id].ContainsKey(type))
            {
                _events[id][type] -= callback;
            }
        }

        public void AddTarget(AddTargetInfo info)
        {
            if (Targets.ContainsKey(info.Id))
            {
                Debug.LogWarning($"Can`t create target with Id {info.Id} Type {info.Type}. Already existed.");
                return;
            }

            ITarget target = TargetsDB.GetTargetFactory(info.Type).GetNewTarget(_warmupContainer);
            if (target == null)
            {
                Debug.LogWarning($"Can`t find target in DB with type {info.Type} Id={info.Id}.");
                return;
            }

            // if (target is BaseMonoTarget monoTarget)
            // {
            //     monoTarget.transform.SetParent(_warmupContainer);
            // }

            // BaseMonoTarget monoTarget = Instantiate<BaseMonoTarget>(target, _container);

            Targets.Add(info.Id, target);

            if (_events.ContainsKey(info.Id) && _events[info.Id].ContainsKey(TargetEventType.Spawned))
            {
                _events[info.Id][TargetEventType.Spawned]?.Invoke(info.Id);
            }

            target.Init(info.Parameters);
            CoroutineSource.StartCoroutine(AddTarget(info.Id, target));
        }

        public void RemoveTarget(string id)
        {
            if (Targets.ContainsKey(id))
            {
                ITarget target = Targets[id];
                Targets.Remove(id);
                target.Realise();
                CoroutineSource.StartCoroutine(RemoveTarget(id, target));
            }
            else
            {
                Debug.LogWarning($"Can`t remove target with Id {id}. Not existed.");
            }
        }

        private IEnumerator AddTarget(string id, ITarget target)
        {
            if (_events.ContainsKey(id) && _events[id].ContainsKey(TargetEventType.StartSpawnAnimation))
            {
                _events[id][TargetEventType.StartSpawnAnimation]?.Invoke(id);
            }

            if (target is BaseMonoTarget monoTarget)
            {
                yield return PlayAndWaitAnim(monoTarget, TargetAnimationTriggerType.WarmUp);

                monoTarget.transform.SetParent(_container);
                monoTarget.ResetPosition();

                yield return PlayAndWaitAnim(monoTarget, TargetAnimationTriggerType.Spawn);


                yield return PlayAndWaitAnim(monoTarget, TargetAnimationTriggerType.New);
            }

            if (_events.ContainsKey(id) && _events[id].ContainsKey(TargetEventType.EndSpawnAnimation))
            {
                _events[id][TargetEventType.EndSpawnAnimation]?.Invoke(id);
            }
        }

        private IEnumerator RemoveTarget(string id, ITarget target)
        {
            if (_events.ContainsKey(id) && _events[id].ContainsKey(TargetEventType.StartDestroyAnimation))
            {
                _events[id][TargetEventType.StartDestroyAnimation]?.Invoke(id);
            }

            if (target is BaseMonoTarget monoTarget)
            {
                yield return PlayAndWaitAnim(monoTarget, TargetAnimationTriggerType.Destroy);
            }

            if (_events.ContainsKey(id) && _events[id].ContainsKey(TargetEventType.EndDestroyAnimation))
            {
                _events[id][TargetEventType.EndDestroyAnimation]?.Invoke(id);
            }

            if (target is BaseMonoTarget monoTarget2)
            {
                Destroy(monoTarget2.gameObject);
            }

            if (_events.ContainsKey(id) && _events[id].ContainsKey(TargetEventType.Destroyed))
            {
                _events[id][TargetEventType.Destroyed]?.Invoke(id);
            }
        }

        private IEnumerator PlayAndWaitAnim(BaseMonoTarget target, TargetAnimationTriggerType anim)
        {
            var waitForEndDestroyAnimation = new WaitForCallBack();
            target.AddAnimationEvent(anim, waitForEndDestroyAnimation.CallBack);

            target.SetAnimationTrigger(anim);

            yield return waitForEndDestroyAnimation;
            target.RemoveAnimationEvent(anim, waitForEndDestroyAnimation.CallBack);
        }

        public IPointer GetPointer(PointerType type, int index)
        {
            // Input.touches[0].
            return pointersDict.ContainsKey(type) && pointersDict[type].ContainsKey(index)
                ? pointersDict[type][index]
                : null;
        }

        public ITarget GetTarget(string id)
        {
            return Targets.ContainsKey(id) ? Targets[id] : null;
        }

        public T GetTarget<T>(string id) where T : ITarget
        {
            return (T) GetTarget(id);
        }

        private void LateUpdate()
        {
            foreach (var pointer in pointersDict.SelectMany(pair => pair.Value, (pair, valuePair) => valuePair.Value))
            {
                pointer.Update();
            }

            // foreach (var idTargetPair in Targets)
            // {
            //     var id = idTargetPair.Key;
            //     var target = idTargetPair.Value;
            //     var oldCondition = target.Condition;
            //     target.CheckCondition();
            //     if (oldCondition != target.Condition)
            //     {
            //         var targetEventType = target.Condition ? TargetEventType.Selected : TargetEventType.Destroyed;
            //         if (_events.ContainsKey(id) && _events[id].ContainsKey(targetEventType))
            //         {
            //             _events[id][targetEventType]?.Invoke();
            //         }
            //     }
            // }

            foreach (var idTargetEventType in Targets
                .Where(pair =>
                {
                    var oldCondition = pair.Value.Condition;
                    if (oldCondition)
                    {
                        return false;
                    }

                    pair.Value.CheckCondition();
                    return oldCondition != pair.Value.Condition;
                })
                .Select(pair =>
                    (pair.Key, pair.Value.Condition ? TargetEventType.Selected : TargetEventType.Deselected,
                        pair.Value))
                .ToList().Where(idTargetEventType => _events.ContainsKey(idTargetEventType.Item1)
                                                     && _events[idTargetEventType.Item1]
                                                         .ContainsKey(idTargetEventType.Item2)))
            {
                if (idTargetEventType.Item3 is BaseMonoTarget monoTarget)
                {
                    CoroutineSource.StartCoroutine(
                        UpdateTargetState(
                            monoTarget,
                            idTargetEventType.Item2 == TargetEventType.Selected
                                ? TargetAnimationTriggerType.Select
                                : TargetAnimationTriggerType.Missed
                        )
                    );
                }

                _events[idTargetEventType.Item1][idTargetEventType.Item2]?.Invoke(idTargetEventType.Item1);
            }
        }

        private IEnumerator UpdateTargetState(BaseMonoTarget monoTarget, TargetAnimationTriggerType animation)
        {
            yield return PlayAndWaitAnim(monoTarget, animation);
        }

        public void Clear()
        {
            foreach (var target in Targets.Values.ToList())
            {
                target.Realise();
                if (target is BaseMonoTarget monoTarget)
                {
                    Destroy(monoTarget.gameObject);
                }
            }

            Targets.Clear();

            _events.Clear();
        }
    }
}
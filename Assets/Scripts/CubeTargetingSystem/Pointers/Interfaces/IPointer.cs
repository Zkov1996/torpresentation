﻿using UnityEngine;

namespace CubeTargetingSystem.Pointers.Interfaces
{
    public interface IPointer
    {
        PointerType Type { get; }
        int Index { get; }
        Vector2 Position { get; }
        PointerInteractionType InteractionType { get; }

        void Update();
    }
}
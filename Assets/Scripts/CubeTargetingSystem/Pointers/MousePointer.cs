﻿using CubeTargetingSystem.Pointers.Interfaces;
using UnityEngine;

namespace CubeTargetingSystem.Pointers
{
    public class MousePointer : IPointer
    {
        private int _index;
        public PointerType Type { get; } = PointerType.Touch;
        public int Index => _index;
        public Vector2 Position { get; private set; }
        public PointerInteractionType InteractionType { get; private set; }

        public MousePointer(int index)
        {
            _index = index;
        }

        public void Update()
        {
            if (Input.GetMouseButtonDown(_index))
            {
                Position = Input.mousePosition;
                InteractionType = PointerInteractionType.Pressed;
            }
            else if (Input.GetMouseButtonUp(_index))
            {
                Position = Input.mousePosition;
                InteractionType = PointerInteractionType.Realised;
            }
        }
    }
}
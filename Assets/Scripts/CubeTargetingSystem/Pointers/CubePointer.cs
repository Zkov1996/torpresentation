﻿using CubeTargetingSystem.Pointers.Interfaces;
using UnityEngine;

namespace CubeTargetingSystem.Pointers
{
    public class CubePointer : IPointer
    {
        private int _index;

        public PointerType Type { get; } = PointerType.Cube;
        public int Index => _index;
        public Vector2 Position { get; private set; }
        public PointerInteractionType InteractionType { get; private set; }

        public CubePointer(int index)
        {
            _index = index;
        }

        public void Update()
        {
            // if (Input.GetKey((KeyCode) (_index + (int) KeyCode.Alpha0)))
            // {
                if (Input.GetMouseButtonDown(0))
                {
                    Position = Input.mousePosition;
                    InteractionType = PointerInteractionType.Pressed;
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    Position = Input.mousePosition;
                    InteractionType = PointerInteractionType.Realised;
                }
            // }
        }
    }
}
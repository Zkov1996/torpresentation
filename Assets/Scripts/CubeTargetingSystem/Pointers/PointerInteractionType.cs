﻿namespace CubeTargetingSystem.Pointers
{
    public enum PointerInteractionType
    {
        Realised,
        Pressed
    }
}
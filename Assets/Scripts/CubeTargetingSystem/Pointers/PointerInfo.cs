﻿using CubeTargetingSystem.Pointers.Interfaces;

namespace CubeTargetingSystem.Pointers
{
    public class PointerInfo
    {
        public PointerType Type;
        public int Index;

        public PointerInteractionType InteractionType;
        public IPointer Pointer;

        public PointerInfo(PointerType type, int index, PointerInteractionType interactionType, IPointer pointer)
        {
            Type = type;
            Index = index;
            InteractionType = interactionType;
            Pointer = pointer;
        }
    }
}
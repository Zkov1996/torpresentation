﻿namespace CubeTargetingSystem.Infos
{
    public enum TargetEventType
    {
        Spawned,
        StartSpawnAnimation,
        EndSpawnAnimation,
        Selected,
        Deselected,
        StartDestroyAnimation,
        EndDestroyAnimation,
        Destroyed
    }
}
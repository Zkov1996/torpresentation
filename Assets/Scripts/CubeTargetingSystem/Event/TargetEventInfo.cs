﻿using System;
using CubeTargetingSystem.Infos;
using UnityEngine.Events;

namespace CubeTargetingSystem.Event
{
    [Serializable]
    public struct TargetEventInfo
    {
        public string Id;
        public TargetEventType Type;
        public UnityEvent<string> Event;

        public TargetEventInfo(string id, TargetEventType type, UnityEvent<string> @event)
        {
            Id = id;
            Type = type;
            Event = @event;
        }
    }
}
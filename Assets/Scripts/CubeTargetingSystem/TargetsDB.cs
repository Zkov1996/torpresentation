﻿using System.Collections.Generic;
using System.Linq;
using CubeTargetingSystem.Fabric;
using CubeTargetingSystem.Targets;
using CubeTargetingSystem.Targets.Realization.AnimationWaiter;
using CubeTargetingSystem.Targets.Realization.Touch;
using UnityEngine;

namespace CubeTargetingSystem
{
    [CreateAssetMenu(fileName = TargetsDB.RESOURCE_PATH, menuName = "Zkov/Crete TargetingDB")]
    public class TargetsDB : ScriptableObject
    {
        private const string RESOURCE_PATH = "TargetsDB";

        public List<BaseMonoTarget> GOTargets;

        private List<AbstractTargetFabric> Targets = new List<AbstractTargetFabric>()
        {
            new ClassTargetFabric<TouchTarget>(TargetType.Touch),
            new ClassTargetFabric<SlideAnimationWaiterTarget>(TargetType.SlideAnimationWaiter),
        };

        private Dictionary<TargetType, AbstractTargetFabric> _targetsDict;

        private Dictionary<TargetType, AbstractTargetFabric> TargetsDict =>
            _targetsDict ??= Targets.Concat(GOTargets.Select(target => new MonoTargetFabric(target)))
                .ToDictionary(fabric => fabric.Type);

        public static TargetsDB Load() => Resources.Load<TargetsDB>(RESOURCE_PATH);

        public AbstractTargetFabric GetTargetFactory(TargetType type)
        {
            return TargetsDict.ContainsKey(type) ? TargetsDict[type] : null;
        }
    }
}
﻿using CubeTargetingSystem.Targets;
using CubeTargetingSystem.Targets.Interfaces;
using UnityEngine;

namespace CubeTargetingSystem.Fabric
{
    public class MonoTargetFabric:AbstractTargetFabric
    {
        private BaseMonoTarget _target;
        public override TargetType Type { get; }


        public MonoTargetFabric(BaseMonoTarget target)
        {
            _target = target;
            Type = target.Type;
        }

        public override ITarget GetNewTarget(RectTransform container)
        {
            return Object.Instantiate(_target, container, false);
        }
    }
}
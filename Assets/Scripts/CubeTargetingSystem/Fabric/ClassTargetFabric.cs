﻿using System;
using CubeTargetingSystem.Targets;
using CubeTargetingSystem.Targets.Interfaces;
using UnityEngine;

namespace CubeTargetingSystem.Fabric
{
    public class ClassTargetFabric<T> : AbstractTargetFabric where T : ITarget
    {
        public ClassTargetFabric(TargetType type)
        {
            Type = type;
        }

        public override TargetType Type { get; }

        public override ITarget GetNewTarget(RectTransform container)
        {
            return Activator.CreateInstance<T>();
        }
    }

}
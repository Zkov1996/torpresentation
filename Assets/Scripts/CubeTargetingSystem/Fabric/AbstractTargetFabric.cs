﻿using CubeTargetingSystem.Targets;
using CubeTargetingSystem.Targets.Interfaces;
using UnityEngine;

namespace CubeTargetingSystem.Fabric
{
    public abstract class AbstractTargetFabric
    {
        public abstract TargetType Type { get; }
        public abstract ITarget GetNewTarget(RectTransform container);
    }

    // public abstract class AbstractTargetFabric<T> : AbstractTargetFabric where T : InstanceParameter
    // {
    //     public override ITarget GetNewTarget(InstanceParameter parameter)
    //     {
    //         return GetNewTarget((T) parameter);
    //     }
    //
    //     protected abstract ITarget GetNewTarget(T parameter);
    // }

    // public class InstanceParameter
    // {
    // }
}
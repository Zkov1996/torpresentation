﻿namespace CubeTargetingSystem.Targets
{
    public enum TargetType
    {
        Slot,
        Zone,
        SlideAnimationWaiter,
        Touch,
    }
}
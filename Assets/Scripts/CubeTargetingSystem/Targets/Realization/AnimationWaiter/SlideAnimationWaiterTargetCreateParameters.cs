﻿using Screens;

namespace CubeTargetingSystem.Targets.Realization.AnimationWaiter
{
    public class SlideAnimationWaiterTargetCreateParameters : BaseTargetCreateParameters
    {
        public Slide Slide;
        public Slide.AnimationType Animation;
    }
}
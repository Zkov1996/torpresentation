﻿using Screens;

namespace CubeTargetingSystem.Targets.Realization.AnimationWaiter
{
    public class SlideAnimationWaiterTarget : BaseTarget<SlideAnimationWaiterTargetCreateParameters>
    {
        public override TargetType Type { get; }

        private Slide _slide;
        private Slide.AnimationType _animation;

        private bool _triggered;

        protected override bool GetCondition()
        {
            return _triggered;
        }

        public override void Realise()
        {
        }

        private void OnTrigger()
        {
            _triggered = true;
            _slide.RemoveAnimationEvent(_animation, OnTrigger);
        }

        protected override void Init(SlideAnimationWaiterTargetCreateParameters createData)
        {
            _slide = createData.Slide;
            _animation = createData.Animation;
            _slide.AddAnimationEvent(_animation, OnTrigger);
        }
    }
}
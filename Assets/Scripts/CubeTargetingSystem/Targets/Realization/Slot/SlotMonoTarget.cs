﻿using CubeTargetingSystem.Pointers;
using CubeTargetingSystem.Unity.Slot;
using Translations.Behaviours;
using UnityEngine;
using PointerType = CubeTargetingSystem.Pointers.PointerType;

namespace CubeTargetingSystem.Targets.Realization.Slot
{
    public class SlotMonoTarget : BaseMonoTarget<SlotTargetCreateParameters>
    {
        [SerializeField] private RectTransform _selfRectTransform;
        [SerializeField] private TMPLocalizer _lable;

        public override void Init()
        {
            // var pos = TargetingSystem.Instance.Container.InverseTransformPoint(createData.Pivot.rect.center);
            // _selfRectTransform.anchoredPosition = createData.AnchoredTransform.rect.center;

            
            _lable.key = _createData.LangKey;
        }

        protected override bool GetCondition()
        {
            var pointerInfo = TargetingSystem.Instance.GetPointer(PointerType.Cube, _createData.PointerId);

            return
                pointerInfo?.InteractionType == PointerInteractionType.Pressed
                && _selfRectTransform.rect.Contains(_selfRectTransform.InverseTransformPoint(pointerInfo.Position));
        }

        protected override void OnRealise()
        {
        }

        public override void ResetPosition()
        {
            _selfRectTransform.position = _createData.AnchoredTransform.position;
        }

        private void LateUpdate()
        {
            // _selfRectTransform.position = _anchoredTransform.position;
        }
    }
}
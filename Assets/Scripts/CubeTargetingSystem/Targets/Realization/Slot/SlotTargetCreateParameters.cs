﻿using CubeTargetingSystem.Targets;
using UnityEngine;

namespace CubeTargetingSystem.Unity.Slot
{
    public class SlotTargetCreateParameters : BaseTargetCreateParameters, IPointeredTargetCreateParameters, IAnchoredTargetCreateParameters
    {
        [SerializeField] private int _pointerId;
        public int PointerId => _pointerId;
        
        [SerializeField] private RectTransform _anchoredTransform;
        public RectTransform AnchoredTransform => _anchoredTransform;
        
        public string LangKey;
    }
}
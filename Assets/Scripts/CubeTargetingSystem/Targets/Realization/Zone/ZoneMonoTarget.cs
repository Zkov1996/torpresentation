﻿using CubeTargetingSystem.Pointers;
using Translations.Behaviours;
using UnityEngine;
using PointerType = CubeTargetingSystem.Pointers.PointerType;

namespace CubeTargetingSystem.Targets.Realization.ZoneTarget
{
    public class ZoneMonoTarget : BaseMonoTarget<ZoneTargetCreateParameters>
    {
        [SerializeField] private RectTransform _selfRectTransform;
        [SerializeField] private UICircle _zone;
        [SerializeField] private RectTransform _cubeMark;
        [SerializeField] private CanvasGroup _cubeMarkCG;
        [SerializeField] private TMPLocalizer _lable;
        [SerializeField] private TMPLocalizer _lableMark;

        public RectTransform TrigPos = null;

        protected override bool GetCondition()
        {
            var pointerInfo = TargetingSystem.Instance.GetPointer(PointerType.Cube, _createData.PointerId);

            var inverseTransformPoint = (Vector2) _selfRectTransform.InverseTransformPoint(pointerInfo.Position);
            float pointerMagnitude = inverseTransformPoint.magnitude;

            bool condition =
                pointerInfo?.InteractionType == PointerInteractionType.Pressed
                && _createData.Size / 2f - _createData.MaxWidth <= pointerMagnitude &&
                pointerMagnitude <= _createData.Size / 2f;

            if (condition)
            {
                TrigPos = _cubeMark;
                if (_createData.PoseSticky)
                {
                    _cubeMark.localPosition = 
                        inverseTransformPoint.normalized
                        // Vector2.up
                        * (_createData.Size - _createData.MaxWidth) * 0.5f;
                }
                else
                {
                    _cubeMark.position = pointerInfo.Position;
                }
            }
            else
            {
                TrigPos = null;
            }

            return condition;
        }

        protected override void OnRealise()
        {
        }

        public override void ResetPosition()
        {
            _selfRectTransform.position = _createData.AnchoredTransform.position;
        }

        public override void Init()
        {
            // _selfRectTransform.position = createData.AnchoredTransform.position;
            _zone.Width = _createData.MaxWidth;
            _zone.SetSize(_createData.Size);
            _lable.gameObject.SetActive(!_createData.WithoutText);
            _lable.key = _createData.LangKey;
            _lableMark.key = _createData.LangKey;
            _cubeMarkCG.alpha = _createData.ShowPointer ? 1 : 0;
        }
    }
}
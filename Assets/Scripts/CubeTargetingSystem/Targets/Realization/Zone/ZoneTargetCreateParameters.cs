﻿using UnityEngine;

namespace CubeTargetingSystem.Targets.Realization.ZoneTarget
{
    public class ZoneTargetCreateParameters : BaseTargetCreateParameters, IPointeredTargetCreateParameters, IAnchoredTargetCreateParameters
    {
        [SerializeField] private int _pointerId;
        public int PointerId => _pointerId;
        
        [SerializeField] private RectTransform _anchoredTransform;
        public RectTransform AnchoredTransform => _anchoredTransform;
        
        public float MaxWidth;
        public float Size;
        public string LangKey;
        public bool WithoutText;
        public bool ShowPointer;
        public bool PoseSticky;
    }
}
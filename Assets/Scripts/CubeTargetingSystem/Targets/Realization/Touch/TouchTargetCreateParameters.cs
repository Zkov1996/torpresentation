﻿using UnityEngine;

namespace CubeTargetingSystem.Targets.Realization.Touch
{
    public class TouchTargetCreateParameters : BaseTargetCreateParameters, IPointeredTargetCreateParameters
    {
        [SerializeField] private RectTransform _rectTransform;
        public RectTransform RectTransform => _rectTransform;
        
        [SerializeField] private int _pointerId;
        public int PointerId => _pointerId;
    }
}
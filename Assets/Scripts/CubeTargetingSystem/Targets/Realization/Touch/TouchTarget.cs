﻿using CubeTargetingSystem.Pointers;
using UnityEngine;
using PointerType = CubeTargetingSystem.Pointers.PointerType;

namespace CubeTargetingSystem.Targets.Realization.Touch
{
    public class TouchTarget : BaseTarget<TouchTargetCreateParameters>
    {
        public override TargetType Type { get; }
        
        private int _pointerId = 0;
        private RectTransform _selfRectTransform;

        protected override bool GetCondition()
        {
            var pointerInfo = TargetingSystem.Instance.GetPointer(PointerType.Touch, _pointerId);

            return pointerInfo?.InteractionType == PointerInteractionType.Pressed
                   && _selfRectTransform.rect.Contains(_selfRectTransform.InverseTransformPoint(pointerInfo.Position));
        }

        protected override void Init(TouchTargetCreateParameters createData)
        {
            _selfRectTransform = createData.RectTransform;
            _pointerId = createData.PointerId;
        }

        public override void Realise()
        {
        }
    }
}
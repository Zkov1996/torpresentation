﻿using System;

namespace CubeTargetingSystem.Targets.Interfaces
{
    public interface ITarget
    {
        TargetType Type { get; }
        bool Condition { get; }
        void CheckCondition();
        void Init(ITargetCreateParameters createData);
        void Realise();
    }

    public interface IGOTarget : ITarget
    {
        void SetAnimationTrigger(TargetAnimationTriggerType type);
        void AddAnimationEvent(TargetAnimationTriggerType type, Action onAnimationEnd);
        void RemoveAnimationEvent(TargetAnimationTriggerType type, Action onAnimationEnd);

        void ResetPosition();
    }
}
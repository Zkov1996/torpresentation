﻿using UnityEngine;

namespace CubeTargetingSystem.Targets
{
    public interface ITargetCreateParameters
    {
    }

    public interface IPointeredTargetCreateParameters : ITargetCreateParameters
    {
        int PointerId { get; }
    }

    public interface IAnchoredTargetCreateParameters : ITargetCreateParameters
    {
        RectTransform AnchoredTransform { get; }
    }
}
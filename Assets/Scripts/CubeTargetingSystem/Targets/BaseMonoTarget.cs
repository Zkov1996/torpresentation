﻿using System;
using System.Collections.Generic;
using System.Linq;
using CubeTargetingSystem.Targets.Interfaces;
using UnityEngine;

namespace CubeTargetingSystem.Targets
{
    public abstract class BaseMonoTarget : MonoBehaviour, IGOTarget
    {
        [SerializeField] private TargetType _type;
        [SerializeField] private Animator _animator;
        [SerializeField] private List<AnimationTriggerContainer> _animationTriggers;
        public TargetType Type => _type;
        public bool Condition { get; private set; }

        private Dictionary<TargetAnimationTriggerType, int> _triggersDict;

        private Dictionary<TargetAnimationTriggerType, int> TriggersDict =>
            _triggersDict ??= _animationTriggers.ToDictionary(
                container => container.Type,
                container => Animator.StringToHash(container.TriggerName)
            );

        private Dictionary<TargetAnimationTriggerType, Action> _onAnimationEndEvent =
            new Dictionary<TargetAnimationTriggerType, Action>();


        public void CheckCondition()
        {
            Condition = GetCondition();
        }

        protected abstract bool GetCondition();

        public abstract void Init(ITargetCreateParameters createData);

        public abstract void Realise();

        public void SetAnimationTrigger(TargetAnimationTriggerType type)
        {
            foreach (var keyValuePair in TriggersDict.Where(pair => pair.Key != type))
            {
                _animator.ResetTrigger(keyValuePair.Value);
            }

            _animator.SetTrigger(TriggersDict[type]);
        }

        public void AddAnimationEvent(TargetAnimationTriggerType animationType, Action onAnimationEnd)
        {
            if (onAnimationEnd != null)
            {
                if (!_onAnimationEndEvent.ContainsKey(animationType))
                {
                    _onAnimationEndEvent[animationType] = null;
                }

                _onAnimationEndEvent[animationType] += onAnimationEnd;
            }
        }

        public void RemoveAnimationEvent(TargetAnimationTriggerType animationType, Action onAnimationEnd)
        {
            if (onAnimationEnd != null)
            {
                if (!_onAnimationEndEvent.ContainsKey(animationType))
                {
                    _onAnimationEndEvent[animationType] = null;
                }

                _onAnimationEndEvent[animationType] -= onAnimationEnd;
            }
        }

        public void OnAnimationEnd(TargetAnimationTriggerType type)
        {
            if (_onAnimationEndEvent.ContainsKey(type))
            {
                _onAnimationEndEvent[type]?.Invoke();
            }
        }

        public abstract void ResetPosition();

        [Serializable]
        public class AnimationTriggerContainer
        {
            public TargetAnimationTriggerType Type;
            public string TriggerName;
        }
    }

    public abstract class BaseMonoTarget<T> : BaseMonoTarget where T : ITargetCreateParameters
    {
        protected T _createData;

        public override void Init(ITargetCreateParameters createData)
        {
            _createData = (T) createData;
            Init();
        }

        public abstract void Init();

        public override void Realise()
        {
            OnRealise();
            _createData = default(T);
        }

        protected abstract void OnRealise();
    }
}
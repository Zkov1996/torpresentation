﻿using CubeTargetingSystem.Targets.Interfaces;

namespace CubeTargetingSystem.Targets
{
    public abstract class BaseTarget : ITarget
    {
        public abstract TargetType Type { get; }
        public bool Condition { get; private set; }

        public void CheckCondition()
        {
            Condition = GetCondition();
        }

        protected abstract bool GetCondition();

        public abstract void Init(ITargetCreateParameters createData);

        public abstract void Realise();
    }

    public abstract class BaseTarget<T> : BaseTarget where T : ITargetCreateParameters
    {
        public override void Init(ITargetCreateParameters createData)
        {
            Init((T) createData);
        }

        protected abstract void Init(T createData);
    }
}
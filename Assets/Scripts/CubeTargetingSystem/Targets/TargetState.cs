﻿namespace CubeTargetingSystem.Targets
{
    public enum TargetState
    {
        New,
        Selected,
        Missed
    }
}
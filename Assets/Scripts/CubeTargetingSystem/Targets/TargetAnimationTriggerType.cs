﻿namespace CubeTargetingSystem.Targets
{
    public enum TargetAnimationTriggerType
    {
        Spawn,
        Destroy,
        New,
        Select,
        Missed,
        WarmUp
    }
}
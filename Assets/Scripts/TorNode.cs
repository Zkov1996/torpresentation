﻿using System.Collections.Generic;
using UnityEngine;

public class TorNode:MonoBehaviour
{
    public List<UICircle> Circles;

    public Color Color = Color.white;
    private void Update()
    {
        foreach (var uiCircle in Circles)
        {
            uiCircle.color = Color;
        }
    }
}
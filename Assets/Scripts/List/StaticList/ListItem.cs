﻿using System.Collections.Generic;
using TMPro;
using Translations.Behaviours;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ListItem : TMPLocalizer
{
    [SerializeField] private TextMeshProUGUI _textTMP;
    [SerializeField] private List<Graphic> _graphics = new();

    protected override TextMeshProUGUI _Text => _textTMP;

    public void SetData(ListItemData data)
    {
        SetKey(data.localizeKey);
        SetColor(data.color);
    }

    public void SetColor(Color color)
    {
        foreach (var graphic in _graphics)
        {
            graphic.color = color;
        }
    }
}
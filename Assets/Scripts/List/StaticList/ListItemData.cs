﻿using System;
using UnityEngine;

[Serializable]
public class ListItemData
{
    public string localizeKey;
    public Color color = Color.white;
}
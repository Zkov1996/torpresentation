﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

// [ExecuteInEditMode]
public class LocalizedStaticList : MonoBehaviour
{
    [SerializeField] private RectTransform _container;
    [SerializeField] private ListItem _itemPrefab;

    public List<ListItemData> items = new();
    private List<ListItem> _list = new();

    private void Awake()
    {
        Spawn();
    }

    private void Spawn()
    {
        _list = items.Select(SpawnItem).ToList();
        LayoutRebuilder.MarkLayoutForRebuild(_container);
    }

    private ListItem SpawnItem(ListItemData data)
    {
        var listItem = Instantiate(_itemPrefab, _container);
        listItem.hideFlags =
            HideFlags.NotEditable
            | HideFlags.DontSave
            | HideFlags.HideInHierarchy;
        listItem.SetData(data);
        return listItem;
    }

    private void Update()
    {
        if (_list.Count != items.Count)
        {
            Spawn();
        }
        
        for (var index = 0; index < _list.Count; index++)
        {
            var listItem = _list[index];
            listItem.SetData(items[index]);
        }
        
        LayoutRebuilder.MarkLayoutForRebuild(_container);
    }
}
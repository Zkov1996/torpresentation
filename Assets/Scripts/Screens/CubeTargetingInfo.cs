﻿using System;
using System.Collections.Generic;
using CubeTargetingSystem.Event;
using CubeTargetingSystem.Infos;

namespace Screens
{
    [Serializable]
    public class CubeTargetingInfo
    {
        public List<AddTargetInfo> AddTargetAfterShow;
        public List<string> RemoveTargetBeforeHide;
        public List<TargetEventInfo> OnTargetEvent;
        public List<string> TargetsCondition;
    }
}
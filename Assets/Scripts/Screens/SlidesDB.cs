﻿using System.Collections.Generic;
using UnityEngine;

namespace Screens
{
    [CreateAssetMenu(fileName = SlidesDB.RESOURCE_PATH, menuName = "Zkov/Create ScreensDB")]
    public class SlidesDB : ScriptableObject
    {
        public const string RESOURCE_PATH = "ScreensDB";
        
        public List<Slide> slides;

        // public Dictionary<int, UIScreen> slidesById;

        private void Init()
        {
            // slidesById = slides
        }

        public static SlidesDB Load()
        {
            var screensDB = Resources.Load<SlidesDB>(SlidesDB.RESOURCE_PATH);
            screensDB.Init();
            return screensDB;
        }
    }
}
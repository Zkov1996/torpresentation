﻿using System.Collections;
using System.Linq;
using CoroutineUtils;
using CubeTargetingSystem;
using CubeTargetingSystem.Infos;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Screens
{
    public class SlideManager : MonoBehaviour
    {
        [SerializeField] private RectTransform _container;
        [SerializeField] private RectTransform _hidedContainer;

        public static SlideManager Instance { get; private set; }

        private SlidesDB _slidesDB;
        private SlidesDB SlidesDB => SlidesDB.Load();

        private int _currentScreenIndex = 0;
        private Slide _currentSlide = null;

        private void Awake()
        {
            Instance = this;
            ShowSlide(0);
        }

        private void ShowSlide(int index)
        {
            if (index < 0 || SlidesDB.slides.Count <= index)
            {
                Reset();
                return;
            }
            CoroutineSource.StartCoroutine(ShowSlideCor(index));
        }

        private IEnumerator ShowSlideCor(int index)
        {
            if (index < 0 || SlidesDB.slides.Count <= index)
            {
                Debug.LogWarning($"Can`t show slide with index {index}");
                yield break;
            }

            var newSlide = InstanceSlide(index);

            if (_currentSlide != null)
            {
                var waitForEndOutroAnimation = new WaitForCallBack();

                yield return RemoveSlideTargets(_currentSlide.Targeting);

                RemoveSlideTargetingEvents(_currentSlide.Targeting);

                _currentSlide.AddAnimationEvent(Slide.AnimationType.Outro, waitForEndOutroAnimation.CallBack);
                _currentSlide.SetAnimationTrigger(Slide.AnimationType.Outro);

                yield return waitForEndOutroAnimation;

                _currentSlide.RemoveAnimationEvent(Slide.AnimationType.Outro, waitForEndOutroAnimation.CallBack);

                Destroy(_currentSlide.gameObject);
                _currentSlide = null;
            }

            MoveSlideToNormalContainer(newSlide);
            _currentSlide = newSlide;
            // _currentScreen.gameObject.SetActive(true);
            _currentScreenIndex = index;
            AddSlideTargetingEvents(_currentSlide.Targeting);

            var waitForEndIntroAnimation = new WaitForCallBack();

            _currentSlide.AddAnimationEvent(Slide.AnimationType.Intro, waitForEndIntroAnimation.CallBack);
            _currentSlide.SetAnimationTrigger(Slide.AnimationType.Intro);

            yield return waitForEndIntroAnimation;

            _currentSlide.RemoveAnimationEvent(Slide.AnimationType.Intro, waitForEndIntroAnimation.CallBack);

            yield return AddSlideTargets(_currentSlide.Targeting);

            _currentSlide.SetAnimationTrigger(Slide.AnimationType.Idle);

            OnSlideTargetInCondition(null);
        }

        public void ShowNext()
        {
            ShowSlide(_currentScreenIndex + 1);
        }

        private Slide InstanceSlide(int index)
        {
            var slide = Instantiate(SlidesDB.slides[index], _hidedContainer);
            return slide;
        }

        private void MoveSlideToNormalContainer(Slide slide)
        {
            slide.transform.SetParent(_container, false);
        }
        
        private IEnumerator AddSlideTargets(CubeTargetingInfo info)
        {
            var waiterList = info.AddTargetAfterShow
                .Select(addTargetInfo =>
                    {
                        var waitForEndSpawnAnimation = new WaitForCallBack<string>();
                        TargetingSystem.Instance.AddEvent(
                            addTargetInfo.Id,
                            TargetEventType.EndSpawnAnimation,
                            waitForEndSpawnAnimation.CallBack
                        );
                        TargetingSystem.Instance.AddTarget(addTargetInfo);

                        return (addTargetInfo.Id, waitForEndSpawnAnimation);
                    }
                )
                .ToList();

            foreach (var waiterItem in waiterList)
            {
                yield return waiterItem.waitForEndSpawnAnimation;
                TargetingSystem.Instance.RemoveEvent(
                    waiterItem.Id,
                    TargetEventType.EndSpawnAnimation,
                    waiterItem.waitForEndSpawnAnimation.CallBack
                );
            }
        }

        private IEnumerator RemoveSlideTargets(CubeTargetingInfo info)
        {
            var waiterList = info.RemoveTargetBeforeHide
                .Select(id =>
                    {
                        var waitForEndSpawnAnimation = new WaitForCallBack<string>();
                        TargetingSystem.Instance.AddEvent(
                            id,
                            TargetEventType.EndDestroyAnimation,
                            waitForEndSpawnAnimation.CallBack
                        );
                        TargetingSystem.Instance.RemoveTarget(id);

                        return (id, waitForEndSpawnAnimation);
                    }
                )
                .ToList();

            foreach (var waiterItem in waiterList)
            {
                yield return waiterItem.waitForEndSpawnAnimation;
                TargetingSystem.Instance.RemoveEvent(
                    waiterItem.Item1,
                    TargetEventType.EndSpawnAnimation,
                    waiterItem.Item2.CallBack
                );
            }
        }

        private void AddSlideTargetingEvents(CubeTargetingInfo info)
        {
            foreach (var targetEventInfo in info.OnTargetEvent)
            {
                TargetingSystem.Instance.AddEvent(targetEventInfo);
            }

            foreach (var targetId in info.TargetsCondition)
            {
                TargetingSystem.Instance.AddEvent(targetId, TargetEventType.Selected, OnSlideTargetInCondition);
            }
        }

        private void RemoveSlideTargetingEvents(CubeTargetingInfo info)
        {
            foreach (var targetEventInfo in info.OnTargetEvent)
            {
                TargetingSystem.Instance.RemoveEvent(targetEventInfo);
            }

            foreach (var targetId in info.TargetsCondition)
            {
                TargetingSystem.Instance.RemoveEvent(targetId, TargetEventType.Selected, OnSlideTargetInCondition);
            }
        }

        private void OnSlideTargetInCondition(string targetId)
        {
            if (_currentSlide.Targeting.TargetsCondition.All(id =>
            {
                var target = TargetingSystem.Instance.GetTarget(id);
                return target != null && target.Condition;
            }))
            {
                ShowNext();
            }
        }

        private void ClearTargetingSystem()
        {
            TargetingSystem.Instance.Clear();
        }

        public void Reset()
        {
            SceneManager.LoadScene("Scenes/MainScene");
            // ClearTargetingSystem();
            // ShowSlide(0);
        }
    }
}
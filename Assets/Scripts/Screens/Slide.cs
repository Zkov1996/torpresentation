﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Screens
{
    public class Slide : MonoBehaviour
    {
        // [SerializeField] private List<Target> _targets;
        public CubeTargetingInfo Targeting;

        public Animator Animator;

        private static readonly Dictionary<AnimationType, int> _animationTypeTriggers =
            new Dictionary<AnimationType, int>()
            {
                {AnimationType.WarmUp, Animator.StringToHash("WarmUp")},
                {AnimationType.Intro, Animator.StringToHash("Intro")},
                {AnimationType.Idle, Animator.StringToHash("Idle")},
                {AnimationType.Outro, Animator.StringToHash("Outro")},
            };


        private Dictionary<AnimationType, Action> _onAnimationEndEvent = new Dictionary<AnimationType, Action>();

        public void SetAnimationTrigger(AnimationType animationType)
        {
            foreach (var keyValuePair in _animationTypeTriggers.Where(pair => pair.Key != animationType))
            {
                Animator.ResetTrigger(keyValuePair.Value);
            }
            
            Animator.SetTrigger(_animationTypeTriggers[animationType]);
            // Animator.Play(_animationTypeTriggers[animationType]);
        }

        public void AddAnimationEvent(AnimationType animationType, Action onAnimationEnd)
        {
            if (onAnimationEnd != null)
            {
                if (!_onAnimationEndEvent.ContainsKey(animationType))
                {
                    _onAnimationEndEvent[animationType] = null;
                }

                _onAnimationEndEvent[animationType] += onAnimationEnd;
            }
        }
        
        public void RemoveAnimationEvent(AnimationType animationType, Action onAnimationEnd)
        {
            if (onAnimationEnd != null)
            {
                if (!_onAnimationEndEvent.ContainsKey(animationType))
                {
                    _onAnimationEndEvent[animationType] = null;
                }

                _onAnimationEndEvent[animationType] -= onAnimationEnd;
            }
        }

        // private void OnChangeTargetState(Target target, bool activated)
        // {
        //     ReadyToChangeState();
        // }

        private void ReadyToChangeState()
        {
            // InvokeWithDelayUtil.InvokeWithDelay(3, SlideManager.Instance.ShowNext);
            SlideManager.Instance.ShowNext();
        }

        public enum AnimationType
        {
            WarmUp,
            Intro,
            Idle,
            Outro
        }

        public void OnAnimationEnd(AnimationType type)
        {
            if (_onAnimationEndEvent.ContainsKey(type))
            {
                _onAnimationEndEvent[type]?.Invoke();
            }
        }
    }
}
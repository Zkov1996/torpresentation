﻿using CubeTargetingSystem;
using CubeTargetingSystem.Targets.Realization.ZoneTarget;
using UI;
using UnityEngine;

[ExecuteInEditMode]
public class ConnectionsController:MonoBehaviour
{
    [SerializeField] private ConnectionLine _connection10;
    [SerializeField] private ConnectionLine _connection21;
    [SerializeField] private ConnectionLine _connection32;
    [SerializeField] private RectTransform _lastConnection;

    public string TargetId;

    [Range(0, 1)] public float Progress;

    private void Awake()
    {
        var target = TargetingSystem.Instance?.GetTarget<ZoneMonoTarget>(TargetId);
        _connection32.Target1 = target?.TrigPos;
    }

    private void Update()
    {
        _connection32.Progress = Mathf.Clamp01(Progress * 4f);
        _connection21.Progress = Mathf.Clamp01(Progress * 4f-1);
        _connection10.Progress = Mathf.Clamp01(Progress * 4f-2);
        _lastConnection.localScale = new Vector3(Mathf.Clamp01(Progress * 4f-3), 1, 1);
    }
}
﻿using TMPro;
using UI.Diamond;
using UnityEngine;

[ExecuteInEditMode]
public class Diamond : MonoBehaviour
{
    [SerializeField] private RectTransform _selfTransform;
    
    [SerializeField] private Square _main;
    public SquareData mainSquare;

    [SerializeField] private Square _pulseBorder;
    public SquareData pulseSquare;

    [SerializeField] private TextMeshProUGUI _text;
    public Color textColor = Color.white;

    private void Update()
    {
        OnUpdate();
    }

    protected virtual void OnUpdate()
    {
        _selfTransform.sizeDelta = new Vector2(1, 1) * mainSquare.Size;
        _main.Data = mainSquare;
        _pulseBorder.Data = pulseSquare;
        _text.color = textColor;
    }
}
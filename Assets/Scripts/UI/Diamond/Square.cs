﻿using UnityEngine;
using UnityEngine.UI;

namespace UI.Diamond
{
    public class Square : MonoBehaviour
    {
        [SerializeField] private RectTransform _selfTransform;
        [SerializeField] private Border _border;
        [SerializeField] private Image _fillImage;

        public SquareData Data
        {
            get => new SquareData()
            {
                Size = _selfTransform.sizeDelta.x,
                Width = _border.Width,
                BorderColor = _border.Color,
                FillColor = _fillImage.color
            };
            set
            {
                _selfTransform.sizeDelta = Vector2.one * value.Size;
                _border.Width = value.Width;
                _border.Color = value.BorderColor;
                _fillImage.color = value.FillColor;
            }
        }
    }
}
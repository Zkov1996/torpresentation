﻿using System;
using UnityEngine;

namespace UI.Diamond
{
    [Serializable]
    public struct SquareData
    {
        public float Size;
        public float Width;
        public Color BorderColor;
        public Color FillColor;
    }
}
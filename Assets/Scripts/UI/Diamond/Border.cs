﻿using UnityEngine;
using UnityEngine.UI;

namespace UI.Diamond
{
    public class Border : MonoBehaviour
    {
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private Image _leftImage;
        [SerializeField] private Image _rightImage;
        [SerializeField] private Image _topImage;
        [SerializeField] private Image _bottomImage;

        // private float _size;
        // public float Size
        // {
        //     get => _size;
        //     set
        //     {
        //         _size = value;
        //         _rectTransform.sizeDelta = Vector2.one * value;
        //     }
        // }

        private float _width;

        public float Width
        {
            get => _width;
            set
            {
                _width = value;

                _leftImage.rectTransform.sizeDelta = new Vector2(value, _rectTransform.rect.height - value);
                _rightImage.rectTransform.sizeDelta = new Vector2(value, _rectTransform.rect.height - value);
                _topImage.rectTransform.sizeDelta = new Vector2(_rectTransform.rect.width - value, value);
                _bottomImage.rectTransform.sizeDelta = new Vector2(_rectTransform.rect.width - value, value);
            }
        }

        private Color _color;

        public Color Color
        {
            get => _color;
            set => _leftImage.color = _rightImage.color = _topImage.color = _bottomImage.color = _color = value;
        }
    }
}
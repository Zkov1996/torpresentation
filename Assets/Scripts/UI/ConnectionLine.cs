﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [ExecuteInEditMode]
    public class ConnectionLine : MonoBehaviour
    {
        [SerializeField] private RectTransform _selfRectTransform;

        // [SerializeField] private RectTransform _containerRectTransform;
        [SerializeField] private RectTransform _line;
        [SerializeField] private Image _lineImage;
        [SerializeField] private UICircleRadialFilled _circle;

        public RectTransform Target1;
        public RectTransform Target2;
        [Range(0, 1)] public float Progress;
        public Color Color = Color.white;

        // public float a1;
        // public float a2;
        // public float between;

        private void Update()
        {
            float angle1 = 0;
            float angle2 = 90;
            if (Target1 != null)
            {
                var inverseTransformPoint1 =
                    (Vector2) _selfRectTransform.InverseTransformPoint(
                        Target1.TransformPoint(Target1.rect.center)
                    );
                _selfRectTransform.sizeDelta = Vector2.one * 2 * inverseTransformPoint1.magnitude;
                Vector2 t1 = inverseTransformPoint1.normalized;
                angle1 = Mathf.Acos(Vector2.Dot(Vector2.right, t1)) * Mathf.Sign(t1.y) * 0.5f / Mathf.PI * 360;
            }

            if (Target2 != null)
            {
                var inverseTransformPoint2 =
                    (Vector2) _selfRectTransform.InverseTransformPoint(
                        Target2.TransformPoint(Target2.rect.center)
                    );
                _circle.SetSize(2 * inverseTransformPoint2.magnitude + 0.5f * _circle.Width);
                Vector2 t2 = inverseTransformPoint2.normalized;
                angle2 = Mathf.Acos(Vector2.Dot(Vector2.right, t2)) * Mathf.Sign(t2.y) * 0.5f / Mathf.PI * 360;
            }

            if (Target1 != null && Target2 != null)
            {
                _line.sizeDelta = new Vector2(
                    ((Vector2) _selfRectTransform.InverseTransformPoint(
                        Target1.TransformPoint(Target1.rect.center)
                    )).magnitude
                    -
                    ((Vector2) _selfRectTransform.InverseTransformPoint(
                        Target2.TransformPoint(Target2.rect.center)
                    )).magnitude
                    +_circle.Width*0.5f
                    ,
                    _line.sizeDelta.y
                );
            }


            _selfRectTransform.Rotate(0, 0, angle1);
            float angleBetween = angle2 - angle1;
            _circle.Angle = angleBetween / 360f;

            _line.localScale = new Vector3(Mathf.Clamp01(Progress * 2), 1, 1);
            _circle.Angle = Mathf.Clamp01(Progress * 2 - 1) * (angle2 - angle1) / 360f;

            _lineImage.color = Color;
            _circle.color = Color;
        }
    }
}
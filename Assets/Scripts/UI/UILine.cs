﻿using UnityEngine;
using UnityEngine.UI;

public class UILine : Image
{
    private static readonly int ShiftMaterialVariable = Shader.PropertyToID("_Shift");
    private static readonly int StepMaterialVariable = Shader.PropertyToID("_Step");
    private static readonly int CountMaterialVariable = Shader.PropertyToID("_Count");

    private static Material _lineMaterial = null;
    private static Material lineMaterial => _lineMaterial ??= Resources.Load<Material>("Materials/UI/UILine");

    [Range(0, 1)] public float Shift = 0;
    [Range(0, 1)] public float Step = 0;
    [Range(0, 20)] public float Count = 0;

    protected override void Awake()
    {
        base.Awake();
        material = Instantiate(lineMaterial);
    }
#if UNITY_EDITOR
    protected override void OnValidate()
    {
        base.OnValidate();
        if (lineMaterial != null)
        {
            material = Instantiate(lineMaterial);
        }
    }
#endif

    private void Update()
    {
        material.SetFloat(ShiftMaterialVariable, Shift);
        material.SetFloat(StepMaterialVariable, Step);
        material.SetFloat(CountMaterialVariable, Count);
    }
}
﻿using UnityEngine;
using UnityEngine.UI;

public class UICircleRadialFilled : Image
{
    private static readonly int WidthPixMaterialVariable = Shader.PropertyToID("_WidthPix");
    private static readonly int ROutPixMaterialVariable = Shader.PropertyToID("_ROutPix");
    private static readonly int AngleMaterialVariable = Shader.PropertyToID("_Angle");
    private static readonly int ClockWiseMaterialVariable = Shader.PropertyToID("_ClockWise");

    private static Material _circleMaterial = null;

    private static Material circleMaterial =>
        _circleMaterial ??= Resources.Load<Material>("Materials/UI/UICircleRadialFilled");

    private RectTransform _rectTransform;

    public float Width = 0;
    [Range(-1, 1)] public float Angle = 0;

    protected override void Awake()
    {
        base.Awake();
        material = Instantiate(circleMaterial);
        _rectTransform = GetComponent<RectTransform>();
    }
#if UNITY_EDITOR
    protected override void OnValidate()
    {
        base.OnValidate();
        if (circleMaterial != null)
        {
            material = Instantiate(circleMaterial);
        }

        _rectTransform ??= GetComponent<RectTransform>();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        if (circleMaterial != null)
        {
            material = Instantiate(circleMaterial);
        }

        _rectTransform ??= GetComponent<RectTransform>();
    }
#endif

    private void Update()
    {
        material.SetFloat(WidthPixMaterialVariable, Width);
        material.SetFloat(ROutPixMaterialVariable, rectTransform.rect.width * 0.5f);
        material.SetFloat(AngleMaterialVariable, Angle);
    }

    public void SetSize(float size)
    {
        rectTransform.sizeDelta = Vector2.one * size;
    }
}
﻿using UnityEditor;
using UnityEditor.UI;

namespace UI.Editor
{
    [CustomEditor(typeof(UILine), true)]
    [CanEditMultipleObjects]
    public class LineEditor : ImageEditor
    {
        private SerializedProperty m_Shift;
        private SerializedProperty m_Step;
        private SerializedProperty m_Count;
        // private SerializedProperty m_LineMaterial;
        protected override void OnEnable()
        {
            base.OnEnable();
            m_Shift = serializedObject.FindProperty("Shift");
            m_Step = serializedObject.FindProperty("Step");
            m_Count = serializedObject.FindProperty("Count");
            // m_LineMaterial = serializedObject.FindProperty("_lineMaterial");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();
            // this.DrawDefaultInspector();
            // EditorGUILayout.PropertyField(m_LineMaterial);
            EditorGUILayout.PropertyField(m_Shift);
            EditorGUILayout.PropertyField(m_Step);
            EditorGUILayout.PropertyField(m_Count);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
﻿using UnityEditor;
using UnityEditor.UI;

namespace UI.Editor
{
    [CustomEditor(typeof(UICircle), true)]
    [CanEditMultipleObjects]
    public class CircleEditor : ImageEditor
    {
        private SerializedProperty m_Width;
        private SerializedProperty m_Progress;
        // private SerializedProperty m_CircleMaterial;
        protected override void OnEnable()
        {
            base.OnEnable();
            m_Width = serializedObject.FindProperty("Width");
            m_Progress = serializedObject.FindProperty("Progress");
            // m_CircleMaterial = serializedObject.FindProperty("_circleMaterial");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();
            // this.DrawDefaultInspector();
            // EditorGUILayout.PropertyField(m_CircleMaterial);
            EditorGUILayout.PropertyField(m_Width);
            EditorGUILayout.PropertyField(m_Progress);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
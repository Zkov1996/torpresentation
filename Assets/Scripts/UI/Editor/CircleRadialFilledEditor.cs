﻿using UnityEditor;
using UnityEditor.UI;

namespace UI.Editor
{
    [CustomEditor(typeof(UICircleRadialFilled), true)]
    [CanEditMultipleObjects]
    public class CircleRadialFilledEditor : ImageEditor
    {
        private SerializedProperty m_Width;
        private SerializedProperty m_Angle;
        // private SerializedProperty m_CircleMaterial;
        protected override void OnEnable()
        {
            base.OnEnable();
            m_Width = serializedObject.FindProperty("Width");
            m_Angle = serializedObject.FindProperty("Angle");
            // m_CircleMaterial = serializedObject.FindProperty("_circleMaterial");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();
            // this.DrawDefaultInspector();
            // EditorGUILayout.PropertyField(m_CircleMaterial);
            EditorGUILayout.PropertyField(m_Width);
            EditorGUILayout.PropertyField(m_Angle);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
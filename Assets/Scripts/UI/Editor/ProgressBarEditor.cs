﻿using UnityEditor;

namespace UI.Editor
{
    // [CustomEditor(typeof(ProgressBar), true)]
    [CanEditMultipleObjects]
    public class ProgressBarEditor : UnityEditor.Editor
    {
        private SerializedProperty m_Progress;

        private void OnEnable()
        {
            m_Progress = serializedObject.FindProperty("Progress");
        }
        
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            // this.DrawDefaultInspector();
            serializedObject.Update();
            // EditorGUILayout.PropertyField(m_LineMaterial);
            EditorGUILayout.PropertyField(m_Progress);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
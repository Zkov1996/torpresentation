﻿using UnityEngine;

namespace UI
{
    [ExecuteInEditMode]
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField] private RectTransform _selfTransform;
        [SerializeField] private RectTransform _fillTransform;

        [SerializeField] [Range(0, 1)] private float _progress;

        public float Progress
        {
            get => _progress;
            set => _progress = value;
        }

        private void Update()
        {
            _fillTransform.sizeDelta = new Vector2(_selfTransform.rect.width * _progress, _selfTransform.rect.height);
        }
    }
}
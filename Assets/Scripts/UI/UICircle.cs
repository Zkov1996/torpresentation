﻿using UnityEngine;
using UnityEngine.UI;

public class UICircle : Image
{
    private static readonly int InRadiusMaterialVariable = Shader.PropertyToID("_WidthPix");
    private static readonly int WidthMaterialVariable = Shader.PropertyToID("_ROutPix");

    private static Material _circleMaterial = null;
    private static Material circleMaterial => _circleMaterial ??= Resources.Load<Material>("Materials/UI/UICircle");

    private RectTransform _rectTransform;

    public float Width = 0;
    [Range(0, 1)] public float Progress = 1;

    protected override void Awake()
    {
        base.Awake();
        material = Instantiate(circleMaterial);
        _rectTransform = GetComponent<RectTransform>();
    }
#if UNITY_EDITOR
    protected override void OnValidate()
    {
        base.OnValidate();
        if (circleMaterial != null)
        {
            material = Instantiate(circleMaterial);
        }

        _rectTransform ??= GetComponent<RectTransform>();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        if (circleMaterial != null)
        {
            material = Instantiate(circleMaterial);
        }

        _rectTransform ??= GetComponent<RectTransform>();
    }
#endif

    private void Update()
    {
        material.SetFloat(InRadiusMaterialVariable, Width * Progress);
        material.SetFloat(WidthMaterialVariable, rectTransform.rect.width * 0.5f);
    }

    public void SetSize(float size)
    {
        rectTransform.sizeDelta = Vector2.one * size;
    }
}
﻿using System;
using System.Collections;
using CoroutineUtils;
using UnityEngine;

namespace Utils
{
    public static class InvokeWithDelayUtil
    {
        public static void InvokeWithDelay(float delaySeconds, Action callback)
        {
            CoroutineSource.StartCoroutine(InvokeWithDelayCor(delaySeconds, callback));
        }

        private static IEnumerator InvokeWithDelayCor(float delaySeconds, Action callback)
        {
            yield return new WaitForSeconds(delaySeconds);
            callback?.Invoke();
        }
    }
}
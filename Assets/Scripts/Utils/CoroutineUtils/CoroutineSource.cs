﻿using System.Collections;
using UnityEditor;
using UnityEngine;

namespace CoroutineUtils
{
    //В классах гжде нужна карутина всегда можно вязть её отсюда. Можно запускать и останавливать только свои карутины
    //Метод ОстановитьВсе специально не реализован
    public class CoroutineSource : MonoBehaviour
    {
        private static CoroutineSource _coroutineSource;

        private static CoroutineSource GetMono
        {
            get
            {
                if (ReferenceEquals(_coroutineSource, null))
                {
                    var coroutineSource = new GameObject("CoroutineSource")
                    {
                        hideFlags = HideFlags.HideAndDontSave
                    };
                    _coroutineSource = coroutineSource.AddComponent<CoroutineSource>();
#if UNITY_EDITOR
                    if (EditorApplication.isPlaying)
                    {
#endif
                        DontDestroyOnLoad(_coroutineSource.gameObject);
#if UNITY_EDITOR
                    }
#endif
                }

                return _coroutineSource;
            }
        }

        public new static Coroutine StartCoroutine(IEnumerator routine)
        {
            return ((MonoBehaviour) GetMono).StartCoroutine(routine);
        }

        public new static void StopCoroutine(IEnumerator routine)
        {
            ((MonoBehaviour) GetMono).StopCoroutine(routine);
        }

        public new static void StopCoroutine(Coroutine routine)
        {
            ((MonoBehaviour) GetMono).StopCoroutine(routine);
        }

        private void OnDestroy()
        {
            _coroutineSource = null;
        }
    }
}
﻿using UnityEngine;

namespace CoroutineUtils
{
	public abstract class MyCustomYieldInstruction : CustomYieldInstruction
	{
		protected bool complited { get; set; }

		public override bool keepWaiting => !complited;

		public override void Reset()
		{
			base.Reset();
			complited = false;
		}
	}
}
﻿using UnityEngine;

namespace CoroutineUtils
{
	public class WaitForCallBack : MyCustomYieldInstruction
	{
		public void CallBack()
		{
			complited = true;
		}
	}

	public class WaitForCallBack<T> : MyCustomYieldInstruction
	{
		public T Result { get; private set; }

		public void CallBack(T result)
		{
			complited = true;
			Result = result;
		}
	}

	public class WaitForCallBack<T1, T2> : MyCustomYieldInstruction
	{
		public (T1, T2) Result { get; private set; }

		public void CallBack(T1 result1, T2 result2)
		{
			complited = true;
			Result = (result1, result2);
		}
	}

	public class WaitForCallBack<T1, T2, T3> : MyCustomYieldInstruction
	{
		public (T1, T2, T3) Result { get; private set; }

		public void CallBack(T1 result1, T2 result2, T3 result3)
		{
			complited = true;
			Result = (result1, result2, result3);
		}
	}
}
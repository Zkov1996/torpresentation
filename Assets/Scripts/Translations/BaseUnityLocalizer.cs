﻿using UnityEngine;

namespace Translations
{
	[ExecuteInEditMode]
	public abstract class BaseUnityLocalizer : MonoBehaviour, IUnityRegisterable
	{
		public bool autoTranslateOnStart = true;
		protected TranslationsData Translations => Localizer.TranslationsData;

		private void Awake()
		{
			OnAwake();
		}

		protected virtual void OnAwake()
		{
		}

		private void Start()
		{
			Localizer.OnChangeLanguage += OnChangeLanguage;
			Localizer.OnChangeLocalizationData += OnChangeLocalizationData;

			OnStart();

			if (autoTranslateOnStart)
			{
				UpdateTextInternal();
			}
		}

		protected virtual void OnStart()
		{
		}

		private void OnDestroy()
		{
			Localizer.OnChangeLanguage -= OnChangeLanguage;
			Localizer.OnChangeLocalizationData -= OnChangeLocalizationData;
		}

		protected virtual void OnChangeLanguage()
		{
			UpdateTextInternal();
		}

		protected virtual void OnChangeLocalizationData()
		{
			UpdateTextInternal();
		}

		private void UpdateTextInternal()
		{
			UpdateText();
		}

		protected abstract void UpdateText();
		public abstract void RegisterAssets();
	}
}
﻿using System.Collections.Generic;

namespace Translations.Models.TTH
{
	public class LocalizedTTHBlock
	{
		public Dictionary<string, LocalizedString> Strings;

		public LocalizedTTHBlock(Dictionary<string, LocalizedString> strings)
		{
			Strings = strings;
		}
	}
}
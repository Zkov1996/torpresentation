﻿using System.Collections.Generic;

namespace Translations.Models.TTH
{
	public class LocalizedTTH
	{
		public LocalizedString Description;
		public List<LocalizedTTHBlock> Blocks;

		public LocalizedTTH(LocalizedString description, List<LocalizedTTHBlock> blocks)
		{
			Description = description;
			Blocks = blocks;
		}
	}
}
﻿namespace Translations.Models
{
	public class LocalizedModelSize
	{
		public LocalizedString Width;
		public LocalizedString Height;

		public LocalizedModelSize(LocalizedString width, LocalizedString height)
		{
			Width = width;
			Height = height;
		}

		public static implicit operator string(LocalizedModelSize lModelSize)
		{
			return $"{lModelSize.Width}x{lModelSize.Height}";
		}
	}
}
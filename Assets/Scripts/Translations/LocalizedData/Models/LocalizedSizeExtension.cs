﻿namespace Translations.Models
{
	public static class LocalizedSizeExtension
	{
		public static string ToStringWithLocKey(this LocalizedModelSize lModelSize, string key)
		{
			return $"{lModelSize.Width.ToStringWithLocKey($"{key}.Width")}x{lModelSize.Height.ToStringWithLocKey($"{key}.Width")}";
		}
	}
}
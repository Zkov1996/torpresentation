﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Translations
{
    [Serializable]
    public class TranslationsData
    {
        public string DefaultLanguage = "ru";
        public Dictionary<string, LocalizedString> Strings;

        public TranslationsData() : this(new Dictionary<string, LocalizedString>())
        {
        }

        public TranslationsData(Dictionary<string, LocalizedString> strings)
        {
            Strings = strings;
        }

        public List<(string, LocalizedString)> CollectLocalizedStrings()
        {
            return Enumerable.Empty<(string, LocalizedString)>()
                .Concat(Strings.Select(pair => (pair.Key, pair.Value)))
                .ToList();
        }
    }
}
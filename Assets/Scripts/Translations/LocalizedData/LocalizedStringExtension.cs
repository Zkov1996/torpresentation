﻿using System;
using UnityEngine;

namespace Translations
{
	public static class LocalizedStringExtension
	{
		private const string EMPTY_LOCALIZATION_WITH_KEY_STRING = "Empty localizationKey {0}";
		private const string EMPTY_LOCALIZATION_LANG_STRING = "Empty lang {0} {1}";
		public static string ToStringWithLocKey(this LocalizedString lstr, string key)
		{
			if (lstr == null)
			{
				var message = String.Format(EMPTY_LOCALIZATION_WITH_KEY_STRING, key);
				Debug.LogWarning(message);
				return message;
			}
			
			if (!lstr.HasLang(Localizer.CurrentLanguage))
			{
				var message = String.Format(EMPTY_LOCALIZATION_LANG_STRING, Localizer.CurrentLanguage, key);
				Debug.LogWarning(message);
				return message;
			}

			return lstr;
		}
	}
}
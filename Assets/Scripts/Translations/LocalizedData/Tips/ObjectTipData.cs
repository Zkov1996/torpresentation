﻿namespace Translations.Tips
{
	public class ObjectTipData
	{
		public LocalizedString Title;
		public LocalizedString HandRotating;
		public LocalizedString Gabarits;
		public LocalizedString HandExit;

		public ObjectTipData() : this(
			new LocalizedString(),
			new LocalizedString(),
			new LocalizedString(),
			new LocalizedString()
		)
		{
		}

		public ObjectTipData(LocalizedString title, LocalizedString handRotating, LocalizedString gabarits,
			LocalizedString handExit)
		{
			Title = title;
			HandRotating = handRotating;
			Gabarits = gabarits;
			HandExit = handExit;
		}
	}
}
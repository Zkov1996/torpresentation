﻿namespace Translations.Tips
{
	public class Tips
	{
		public MenuTipData Menu;
		public ObjectTipData Object;
		public VideoPlayersTipData VideoPlayers;

		public Tips() : this(new MenuTipData(), new ObjectTipData(), new VideoPlayersTipData())
		{
		}
		public Tips(MenuTipData menu, ObjectTipData o, VideoPlayersTipData videoPlayers)
		{
			Menu = menu;
			Object = o;
			VideoPlayers = videoPlayers;
		}
	}
}
﻿namespace Translations.Tips
{
	public class MenuTipData
	{
		public LocalizedString Title;
		public LocalizedString FingerPointer;

		public MenuTipData() : this(new LocalizedString(), new LocalizedString())
		{
		}

		public MenuTipData(LocalizedString title, LocalizedString fingerPointer)
		{
			Title = title;
			FingerPointer = fingerPointer;
		}
	}
}
﻿namespace Translations.Tips
{
	public class VideoPlayersTipData
	{
		public LocalizedString FingerPointer;
		public LocalizedString HandRotating;
		public LocalizedString Gabarits;
		public LocalizedString HandExit;

		public VideoPlayersTipData() : this(
			new LocalizedString(),
			new LocalizedString(),
			new LocalizedString(),
			new LocalizedString()
		)
		{
		}

		public VideoPlayersTipData(LocalizedString fingerPointer, LocalizedString handRotating,
			LocalizedString gabarits, LocalizedString handExit)
		{
			FingerPointer = fingerPointer;
			HandRotating = handRotating;
			Gabarits = gabarits;
			HandExit = handExit;
		}
	}
}
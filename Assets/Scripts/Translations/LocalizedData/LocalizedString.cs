﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Translations
{
	public class LocalizedString
	{
		private const string EMPTY_LOCALIZATION_STRING = "Empty localization";
		private const string EMPTY_LOCALIZATION_LANG_STRING = "Empty lang {0}";
		public Dictionary<string, string> Localizations = new Dictionary<string, string>();

		public LocalizedString()
		{
		}

		public LocalizedString(params (string, string)[] pairs)
		{
			Localizations = (pairs ?? Array.Empty<(string, string)>())
				.ToDictionary(tuple => tuple.Item1, tuple => tuple.Item2);
		}

		public static implicit operator string(LocalizedString lstr)
		{
			if (lstr == null)
			{
				Debug.LogWarning(EMPTY_LOCALIZATION_STRING);
				return EMPTY_LOCALIZATION_STRING;
			}

			if (!lstr.HasLang(Localizer.CurrentLanguage))
			{
				var message = String.Format(EMPTY_LOCALIZATION_LANG_STRING, Localizer.CurrentLanguage);
				Debug.LogWarning(message);
				return message;
			}

			return lstr.Localizations[Localizer.CurrentLanguage];
		}

		public bool IsComplete(List<string> langCodes)
		{
			return !GetMissedLangs(langCodes).Any();
		}

		public List<string> GetMissedLangs(List<string> langCodes)
		{
			var langs = Localizations
				.Where(pair => !String.IsNullOrWhiteSpace(pair.Value)).Select(pair => pair.Key);

			return langCodes.Where(langCode => !langs.Contains(langCode)).ToList();
		}

		public bool HasLang(string langCode)
		{
			return Localizations.ContainsKey(langCode) && !String.IsNullOrWhiteSpace(Localizations[langCode]);
		}
	}
}
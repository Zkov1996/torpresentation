﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CoroutineUtils;
using Newtonsoft.Json;
using UnityEngine;

namespace Translations
{
	public static class Localizer
	{
		private const string JSON_LOCALIZE_FILE = "Translations.json";
		private static string JsonFilePath => Path.Combine(Application.streamingAssetsPath, JSON_LOCALIZE_FILE);

		private static string _currentLanguage = null;
		
		private static TranslationsData _translationsData;

		public static TranslationsData TranslationsData
		{
			get
			{
				if (_translationsData == null)
				{
					Load();
				}

				return _translationsData;
			}
		}

		public static string CurrentLanguage => _currentLanguage ??= TranslationsData.DefaultLanguage;
		public static event Action OnChangeLanguage;
		public static event Action OnAfterChangeLanguage;
		public static event Action OnChangeLocalizationData;

		private static Coroutine _scanFileCoroutine = null;

		private static FileScanInfo _scanInfo = new FileScanInfo();

		private static List<LanguageInfo> _languages;

		public static List<LanguageInfo> Languages
		{
			get
			{
				if (_languages == null)
				{
					Load();
				}

				return _languages;
			}
		}

		public static bool SetLanguage(string languageCode)
		{
			_currentLanguage = languageCode;

			OnChangeLanguage?.Invoke();
			OnAfterChangeLanguage?.Invoke();

			return true;
		}

		public static LocalizedString GetString(string key)
		{
			return TranslationsData.Strings.ContainsKey(key) ? TranslationsData.Strings[key] : null;
		}

		public static void Load(bool startScanning = true)
		{
			if (startScanning)
			{
				StartScanFile();
			}
			else
			{
				_translationsData = File.Exists(JsonFilePath)
					? JsonConvert.DeserializeObject<TranslationsData>(File.ReadAllText(JsonFilePath))
					: new TranslationsData();

				_languages = GetAvailableLanguages();
			}
		}

		private static void StartScanFile()
		{
			// _scanInfo.LastWrite = File.GetLastWriteTime(JsonFilePath);
			_scanFileCoroutine ??= CoroutineSource.StartCoroutine(ScanFile());
		}

		private static void StopScanFile()
		{
			if (_scanFileCoroutine == null)
			{
				CoroutineSource.StopCoroutine(_scanFileCoroutine);
			}
		}

		private static IEnumerator ScanFile()
		{
			while (true)
			{
				if (
					!File.Exists(JsonFilePath)
					|| _scanInfo.LastWrite != File.GetLastWriteTime(JsonFilePath)
				)
				{
					_scanInfo.LastWrite = File.GetLastWriteTime(JsonFilePath);
					Load(false);
					OnChangeLocalizationData?.Invoke();
				}

				yield return new WaitForSeconds(1);
			}

			_scanFileCoroutine = null;
		}

		private static List<LanguageInfo> GetAvailableLanguages()
		{
			var collectLocalizedStrings = TranslationsData.CollectLocalizedStrings()
					.Where(tuple => tuple.Item2 != null).ToList()
				;

			var languages = collectLocalizedStrings
				.SelectMany(
					tuple => tuple.Item2.Localizations
						// .Where(pair => String.IsNullOrWhiteSpace(pair.Value))
						.Select(pair => pair.Key)
				)
				.Distinct()
				.ToList();

			var missedLanguageInfos = collectLocalizedStrings
				.SelectMany(
					tuple => tuple.Item2.GetMissedLangs(languages),
					(tuple, s) => (tuple.Item1, tuple.Item2, s)
				)
				.GroupBy(tuple => tuple.Item3)
				.Select(tuples => new LanguageInfo(
						tuples.Key,
						tuples.Select(tuple => tuple.Item1).ToList()
					)
				)
				.ToList();

			var languageInfos = languages
				.Where(s => !missedLanguageInfos.Any(info => info.LanguageCode == s))
				.Select(s => new LanguageInfo(s))
				.Concat(missedLanguageInfos)
				.ToList();

			// var languageInfos = languages
			// 	.Select(languageCode => new LanguageInfo(languageCode))
			// 	.ToList();


			Debug.LogWarning(
				$"Has unlocalized: {String.Join(",\n", missedLanguageInfos.Select(info => $"{info.LanguageCode}: {String.Join(", ", info.MissingInStrings)}"))}");

			return languageInfos;
		}

		private class FileScanInfo
		{
			public DateTime LastWrite = DateTime.Now;
		}
	}
}
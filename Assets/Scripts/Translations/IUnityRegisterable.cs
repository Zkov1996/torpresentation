﻿namespace Translations
{
	public interface IUnityRegisterable
	{
		public void RegisterAssets();
	}
}
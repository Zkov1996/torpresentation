﻿using System.Collections.Generic;
using System.Linq;

namespace Translations
{
	public class LanguageInfo
	{
		public string LanguageCode;
		public bool Full => !MissingInStrings.Any();
		
		public List<string> MissingInStrings;

		public LanguageInfo(string languageCode)
		{
			LanguageCode = languageCode;
			MissingInStrings = new List<string>();
		}
		
		public LanguageInfo(string languageCode, List<string> missingInStrings)
		{
			LanguageCode = languageCode;
			MissingInStrings = missingInStrings;
		}
	}
}
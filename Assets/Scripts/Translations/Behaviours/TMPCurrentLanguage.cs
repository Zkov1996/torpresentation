﻿using TMPro;
using UnityEngine;

namespace Translations.Behaviours
{
    [ExecuteInEditMode]
    public class TMPCurrentLanguage : BaseUnityLocalizer
    {
        [SerializeField] private TextMeshProUGUI _text;

        // public string key;

        public override void RegisterAssets()
        {
            _text ??= gameObject.GetComponent<TextMeshProUGUI>();
        }

        protected override void UpdateText()
        {
            var localizedString = Localizer.GetString(Localizer.CurrentLanguage);
            _text.text = localizedString.ToStringWithLocKey(Localizer.CurrentLanguage);
        }
        
#if UNITY_EDITOR
        protected void OnValidate()
        {
            UpdateText();
        }
#endif
    }
}
﻿using System;
using TMPro;
using UnityEditor;
using UnityEngine;

namespace Translations.Behaviours
{
    [ExecuteInEditMode]
    public class TMPLocalizer : BaseUnityLocalizer
    {
        protected TextMeshProUGUI _text;
        protected virtual TextMeshProUGUI _Text => _text ??= gameObject.GetComponent<TextMeshProUGUI>();

        private string _oldKeykey;
        public string key;


        public void SetKey(string key)
        {
            this.key = key;
        }

        protected override void OnAwake()
        {
            _Text.text = null;
        }

        public override void RegisterAssets()
        {
        }

        protected override void UpdateText()
        {
            var localizedString = Localizer.GetString(key);
            _Text.text = localizedString?.ToStringWithLocKey(key);
        }

        private void Update()
        {
            if (_oldKeykey != key)
            {
                _oldKeykey = key;
                UpdateText();
            }
        }

#if UNITY_EDITOR
        protected void OnValidate()
        {
            if (!EditorApplication.isPlaying && !EditorApplication.isPaused)
                UpdateText();
        }
#endif
    }
}
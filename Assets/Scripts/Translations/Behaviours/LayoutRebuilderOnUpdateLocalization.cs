﻿using UnityEngine;
using UnityEngine.UI;

namespace Translations.Behaviours
{
    public class LayoutRebuilderOnUpdateLocalization : MonoBehaviour
    {
        private RectTransform _container;

        private void Awake()
        {
            _container ??= GetComponent<RectTransform>();
            Localizer.OnAfterChangeLanguage += OnAfterUpdateText;
        }

        private void OnAfterUpdateText()
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(_container);
            // LayoutRebuilder.MarkLayoutForRebuild(_container);
        }
    }
}
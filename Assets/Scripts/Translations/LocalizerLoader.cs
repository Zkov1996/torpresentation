﻿using System.Linq;
using UnityEngine;

namespace Translations
{
	public class LocalizerLoader : MonoBehaviour
	{
		private void Start()
		{
			Localizer.Load();
			PrepareAssets();
		}

		private void PrepareAssets()
		{
			var baseUnityLocalizers = Object.FindObjectsOfType<BaseUnityLocalizer>(true);
			var unityRegisterables = baseUnityLocalizers.OfType<IUnityRegisterable>().ToList();
			foreach (var unityRegisterable in unityRegisterables)
			{
				unityRegisterable.RegisterAssets();
			}
		}
	}
}
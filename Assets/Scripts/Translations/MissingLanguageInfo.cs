﻿namespace Translations
{
	public class MissingLanguageInfo
	{
		public string LanguageCode;
		public string StringKey;
	}
}
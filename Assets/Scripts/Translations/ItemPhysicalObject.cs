﻿using System;
using System.Collections.Generic;
using Translations.Models;
using Translations.Models.TTH;

namespace Translations
{
	[Serializable]
	public class ItemPhysicalObject
	{
		public string nameKey;
	
		public LocalizedString iName;
		public LocalizedString iType;
		public LocalizedString pType;
		public LocalizedModelSize iSize;
		public LocalizedString production;
		public List<LocalizedTTH> tth;
		public List<LocalizedString> dots;
	}

	[Serializable]
	public class ItemPhysicalObjectOld
	{
		public int id;
	
		public string iName;
		public string iType;
		public string pType;
		public string iSize;
		public string ttx;
	}
}